TYPINGTEST
=

About
-

typingtest is a simple tui program to test your typing speed.
It's interface is inspired by codemonkey.com.
The program is very simple ~300 lines of code so you are encouraged to modify it if needed.

Installation
-

```
git clone https://git.chambaz.xyz/typingtest
cd typingtest/build
sudo make clean install
```

Licence
-

This project is licenced under the GPLv2 licence.
For more information, read the LICENSE file.
