VERSION = 1.0
PREFIX ?= /usr/local
MANPREFIX = ${PREFIX}/share/man
TERMINFO := ${DESTDIR}${PREFIX}/share/terminfo
INCS = -I ../include
LIBS = -lncurses
CFLAGS += -std=c99 ${INCS} -DVERSION=\"${VERSION}\" -DNDBUG
LDFLAGS += ${LIBS}
DEBUG_CFLAGS = ${CFLAGS} -UNDEBUG -O0 -g -ggdb -Wall -Wextra -Wno-unused-parameter
CC = clang
STRIP ?= strip
