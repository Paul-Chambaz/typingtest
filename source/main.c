#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <locale.h>
#include <ncurses.h>
#include "wordlist.h"
#include "config.h"

#define ctrl(x) ((x) & 0x1f)

void init_draw (char **main_strings, int line_length, int line_count, int timer, char *notice_text_p1, char *notice_text_p2);
void init_words (int main_word_number, int max_line_length, char ***main_strings, int *line_count, char ***value_char, bool ***value_type);
void draw_line (char **main_strings, char **value_char, int line, int y, int line_length);

int main () {
  char *notice_text_p1 = "Press <C-q> to quit and <C-r> to restart";
  char *notice_text_p2 = "Press -  for less time and + for more";
  bool running = true;
  bool typing = false;
  int line_length = 70;
  int main_word_number = 400;
  int line_count;
  char **main_strings, **value_char;
  bool **value_type;
  clock_t clock_prev, clock_cur;
  int index_orig_timer = 2;
  int orig_timers[] = {5, 10, 15, 30, 45, 60, 120, 300};
  int nb_orig_timer = sizeof(orig_timers) / sizeof(int);
  int orig_timer = orig_timers[index_orig_timer];
  double cpu_time_used, timer = orig_timer;
  int line = 0, pos = 0;
  int key;

  srand((unsigned) time(NULL));

  setlocale(LC_CTYPE, "");

  initscr();
  if (!has_colors()) { endwin(); fprintf(stderr, "Your terminal does not support colors."); exit(EXIT_FAILURE); }
  raw();
  keypad(stdscr, TRUE);
  refresh();
  start_color();
  noecho();
  nonl();
  timeout(0);

  // int line_length = (70 > COLS / 2) ? 70 : COLS / 2;
  line_length = (line_length > COLS / 2) ? line_length : COLS / 2;
  line_length = (line_length < COLS - 10) ? line_length  : COLS - 10;
  line_length = (line_length > 10) ? line_length : 10;

  init_words(main_word_number, line_length, &main_strings, &line_count, &value_char, &value_type);

  init_pair(1, 8, 0);
  init_pair(2, 1, 0);

  init_draw(main_strings, line_length, line_count, timer, notice_text_p1, notice_text_p2);

  while ((key = getch())) {
    if (running) {
      if (typing) {
        clock_cur = clock();
        cpu_time_used = ((double) (clock_cur - clock_prev)) / CLOCKS_PER_SEC;
        clock_prev = clock_cur;
        timer -= cpu_time_used;
        if (timer < 0) {
          running = false;
          int total = 0, total_correct = 0;
          for (int i = 0; i <= line; i++) {
            for (int j = 0; j < strlen(main_strings[line]); ++j) {
              if (value_char[i][j] != -1) {
                total++;
                if (value_type[i][j]) {
                  total_correct++;
                }
              }
            }
          }
          int wpm = (int) (total_correct * 60) / (5 * orig_timer);
          int acc = (int) (100 * (float) total_correct / (float) total);
          clear();
          timeout(-1);
          curs_set(0);
          mvprintw(LINES / 2 - 3, COLS / 2 - 10, "words per minutes: ");
          attron(A_BOLD);
          printw("%d", wpm);
          attroff(A_BOLD);
          mvprintw(LINES / 2 - 2, COLS / 2 - 6, "accuracy: ");
          attron(A_BOLD);
          printw("%d%%", (int) acc);
          attroff(A_BOLD);
          mvprintw(LINES / 2 - 1, COLS / 2 - 4, "time: ");
          attron(A_BOLD);
          printw("%ds", (int) orig_timer);
          attroff(A_BOLD);
          mvprintw(LINES / 2 + 2, (COLS - strlen(notice_text_p1)) / 2, "%s", notice_text_p1);
          mvprintw(LINES / 2 + 3, (COLS - strlen(notice_text_p2)) / 2, "%s", notice_text_p2);
          refresh();
        } else {
          attron(A_BOLD);
          mvprintw(LINES / 2 - 5, (COLS - line_length) / 2, "%02d:%02d", (int) timer, (int) ((timer - (int) timer) * 100));
          attroff(A_BOLD);
          move(LINES / 2 - 3 + ((line == 0) ? 0 : 1), (COLS - line_length) / 2 + pos);
          refresh();
        }
      } else {
        if (key == '-' || key == '_') {
          index_orig_timer--;
          index_orig_timer = (index_orig_timer < 0) ? 0 : index_orig_timer;
          orig_timer = orig_timers[index_orig_timer];
          timer = orig_timer;
          attron(A_BOLD);
          move(LINES / 2 - 5, (COLS - line_length) / 2);
          clrtoeol();
          mvprintw(LINES / 2 - 5, (COLS - line_length) / 2, "%02d:%02d", (int) timer, (int) ((timer - (int) timer) * 100));
          attroff(A_BOLD);
          move(LINES / 2 - 3 + ((line == 0) ? 0 : 1), (COLS - line_length) / 2 + pos);
          refresh();
        } else if (key == '=' || key == '+') {
          index_orig_timer++;
          index_orig_timer = (index_orig_timer > nb_orig_timer - 1) ? nb_orig_timer - 1 : index_orig_timer;
          orig_timer = orig_timers[index_orig_timer];
          timer = orig_timer;
          attron(A_BOLD);
          move(LINES / 2 - 5, (COLS - line_length) / 2);
          clrtoeol();
          mvprintw(LINES / 2 - 5, (COLS - line_length) / 2, "%02d:%02d", (int) timer, (int) ((timer - (int) timer) * 100));
          attroff(A_BOLD);
          move(LINES / 2 - 3 + ((line == 0) ? 0 : 1), (COLS - line_length) / 2 + pos);
          refresh();
        }
      }
      if (key == ctrl('q')) {
        break;
      } else if (key == ctrl('r')) {
        // reset the word list and restart the software
        for (int i = 0; i < line_count; ++i) {
          free(main_strings[i]);
          free(value_char[i]);
        }
        free(main_strings);
        free(value_char);
        init_words(main_word_number, line_length, &main_strings, &line_count, &value_char, &value_type);
        clear();
        refresh();
        timer = orig_timer;
        init_draw(main_strings, line_length, line_count, timer, notice_text_p1, notice_text_p2);
        line = 0;
        pos = 0;
        typing = false;
      } else if ((key >= 'a' && key <= 'z') || (key >= 'A' && key <= 'Z') || (key >= 0 && key <= 9) || (key == ' ')) {
        if (!typing) {
          typing = true;
          timer = orig_timer;
          clock_prev = clock();
        }
        if (key == main_strings[line][pos]) {
          value_char[line][pos] = 1;
          value_type[line][pos] = true;
        } else if (key != main_strings[line][pos]) {
          value_char[line][pos] = 0;
          value_type[line][pos] = false;
        }
        pos++;
        draw_line(main_strings, value_char, line, (line == 0) ? 0 : 1, line_length);
        if (pos == strlen(main_strings[line])) {
          // next line
          line++;
          pos = 0;
          if (line > 1) {
            clear();
            attron(A_BOLD);
            mvprintw(LINES / 2 - 5, (COLS - line_length) / 2, "%02d:%02d", (int) timer, (int) ((timer - (int) timer) * 100));
            attroff(A_BOLD);
            mvprintw(LINES / 2 + 2, (COLS - strlen(notice_text_p1)) / 2, "%s", notice_text_p1);
            mvprintw(LINES / 2 + 3, (COLS - strlen(notice_text_p2)) / 2, "%s", notice_text_p2);
            draw_line(main_strings, value_char, line - 1, 0, line_length);
            draw_line(main_strings, value_char, line, 1, line_length);
            draw_line(main_strings, value_char, line + 1, 2, line_length);
          }
        }
        move(LINES / 2 - 3 + ((line == 0) ? 0 : 1), (COLS - line_length) / 2 + pos);
        refresh();
      } else if (key == KEY_BACKSPACE || key == KEY_DC || key == 127) {
        if (pos == 0) {
          if (line == 0) {
            pos = 1;
          } else {
            line--;
            pos = strlen(main_strings[line]);
          }
        }
        pos--;
        value_char[line][pos] = -1;
        clear();
        attron(A_BOLD);
        mvprintw(LINES / 2 - 5, (COLS - line_length) / 2, "%02d:%02d", (int) timer, (int) ((timer - (int) timer) * 100));
        attroff(A_BOLD);
        mvprintw(LINES / 2 + 2, (COLS - strlen(notice_text_p1)) / 2, "%s", notice_text_p1);
        mvprintw(LINES / 2 + 3, (COLS - strlen(notice_text_p2)) / 2, "%s", notice_text_p2);
        draw_line(main_strings, value_char, line, (line == 0) ? 0 : 1, line_length);
        if (line > 0) {
          draw_line(main_strings, value_char, line - 1, 0, line_length);
          draw_line(main_strings, value_char, line + 1, 2, line_length);
        } else {
          draw_line(main_strings, value_char, line + 1, 1, line_length);
          draw_line(main_strings, value_char, line + 2, 2, line_length);
        }
        move(LINES / 2 - 3 + ((line == 0) ? 0 : 1), (COLS - line_length) / 2 + pos);
        refresh();
      }
    } else {
      if (key == ctrl('q')) {
        break;
      } else if (key == ctrl('r')) {
        // reset the word list and restart the software
        for (int i = 0; i < line_count; ++i) {
          free(main_strings[i]);
          free(value_char[i]);
        }
        free(main_strings);
        free(value_char);
        init_words(main_word_number, line_length, &main_strings, &line_count, &value_char, &value_type);
        clear();
        timeout(0);
        curs_set(1);
        line = 0;
        pos = 0;
        typing = false;
        timer = orig_timer;
        running = true;
        init_draw(main_strings, line_length, line_count, timer, notice_text_p1, notice_text_p2);
        refresh();
      }
    }
  }
  endwin();
  exit(EXIT_SUCCESS);
}

void draw_line (char **main_strings, char **value_char, int line, int y, int line_length) {
  // *writing_window = newwin(3, line_length, LINES / 2 - 2, (COLS - line_length) / 2);
  move(LINES / 2 - 3 + y, (COLS - line_length) / 2);
  for (int i = 0; i < strlen(main_strings[line]); ++i) {
    if (value_char[line][i] == -1) {
      attron(COLOR_PAIR(1));
      addch(main_strings[line][i]);
      attroff(COLOR_PAIR(1));
    } else if (value_char[line][i] == 0) {
      attron(COLOR_PAIR(2));
      attron(A_BOLD);
      addch((main_strings[line][i] == ' ') ? '_' : main_strings[line][i]);
      attroff(A_BOLD);
      attroff(COLOR_PAIR(2));
    } else if (value_char[line][i] == 1) {
      attron(A_BOLD);
      addch(main_strings[line][i]);
      attroff(A_BOLD);
    }
  }
}

void init_words (int main_word_number, int max_line_length, char ***main_strings, int *line_count, char ***value_char, bool ***value_type) {
  // generating the word list

  // maximum word distance between two occurences of the same word
  int max_freq = 20;
  // index of the words fetched
  int index;
  // used for distance between two occurences
  bool diff;
  // array of words used for the test
  char **main_word_list = (char **) malloc(sizeof(char *) * main_word_number);
  // used for generating random number
  double random;
  for (int i = 0; i < main_word_number; i++) {
    do {
      // randomly generating the index
      random = (double) rand() / (RAND_MAX);
      random = random * random;
      index = (int) (random * 256);
      // checking that the chosen words does not appear in the past max_freq words
      diff = true;
      for (int j = 0; j < max_freq; ++j) {
        if (i - j - 1 >= 0 && !strcmp(word_list[index], main_word_list[i - j - 1])) {
          diff = false;
          j = max_freq;
        }
      }
    } while (!diff);
    // finally copying the word from the word list to the list of used words
    main_word_list[i] = (char *) malloc(sizeof(char) * (strlen(word_list[index]) + 1));
    strcpy(main_word_list[i], word_list[index]);
    main_word_list[i][strlen(word_list[index]) + 1] = '\0';
  }

  // generating the lists of lines from the word generated

  // maximum word size used to guess the inital array size before realloc
  int max_word_size = 20;
  int line_length = 0;
  *(main_strings) = (char **) malloc(sizeof(char *) * main_word_number * max_word_size / max_line_length);
  for (int i = 0; i < main_word_number * max_word_size / max_line_length; ++i) {
    (*main_strings)[i] = (char *) malloc(sizeof(char) * (max_line_length + 1));
    (*main_strings)[i][0] = '\0';
  }
  *line_count = 0;
  for (int i = 0; i< main_word_number; ++i) {
    if (strlen((*main_strings)[*line_count]) + strlen(main_word_list[i]) + 1 > max_line_length) {
      // if we are going to be longer than max_line_length
      // we make sure the char array has the correct size and go to the next line
      (*main_strings)[*line_count] = realloc((*main_strings)[*line_count], sizeof(char) * (line_length + 1));
      (*line_count)++;
      line_length = 0;
    } else {
      // if not then we add the word followed by a space
      strcat((*main_strings)[*line_count], main_word_list[i]);
      strcat((*main_strings)[*line_count], " ");
      line_length += strlen(main_word_list[i]) + 1;
      // if we are at the last number we just stop
      if (i == main_word_number - 1) {
        (*line_count)++;
      }
    }
  }

  // free the word list
  for (int i = 0; i < main_word_number; ++i) {
    free(main_word_list[i]);
  }
  free(main_word_list);

  // create the list of state for character -1 untyped, 0 wrong, 1 correct
  // create the list of type false - not already correctly typed, true - already correctly typed
  *value_char = (char **) malloc(sizeof(char *) * *line_count);
  *value_type = (bool **) malloc(sizeof(char *) * *line_count);
  for (int i = 0; i < *line_count; ++i) {
    (*value_char)[i] = (char *) malloc(sizeof(char) * strlen((*main_strings)[i]));
    (*value_type)[i] = (bool *) malloc(sizeof(bool) * strlen((*main_strings)[i]));
    for (char j = 0; j < strlen((*main_strings)[i]); ++j) {
      (*value_char)[i][j] = -1;
      (*value_type)[i][j] = false;
    }
  }
}

void init_draw (char **main_strings, int line_length, int line_count, int timer, char *notice_text_p1, char *notice_text_p2) {
  attron(COLOR_PAIR(1));
  for (int i = 0; i < 3; ++i) {
    mvprintw(LINES / 2 - 3 + i, (COLS - line_length) / 2, "%s", main_strings[i]);
  }
  attroff(COLOR_PAIR(1));

  attron(A_BOLD);
  mvprintw(LINES / 2 - 5, (COLS - line_length) / 2, "%02d:%02d", (int) timer, (int) ((timer - (int) timer) * 100));
  attroff(A_BOLD);

  mvprintw(LINES / 2 + 2, (COLS - strlen(notice_text_p1)) / 2, "%s", notice_text_p1);
  mvprintw(LINES / 2 + 3, (COLS - strlen(notice_text_p2)) / 2, "%s", notice_text_p2);

  move(LINES / 2 - 3, (COLS - line_length) /2);
  refresh();
}
